**Clone Repo**

`git clone https://bitbucket.org/palaganaskurl/ann-php.git`

**Generate Autoload**

`composer dump-autoload`

**Run Logic Gates**

`php logic_gates.php`